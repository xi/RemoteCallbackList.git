package com.example.ee.aidl;

import android.os.Bundle;
import android.os.RemoteException;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void onClick(View view){
        if(view.getId() == R.id.btn_call_remote){
            AidlApp app = (AidlApp) getApplication();
            if (app.remoteService != null){
                try {
                    int pid = app.remoteService.getPid();
                    Log.d(TAG, "onServiceConnected: remote pid = " + pid);
                    app.remoteService.fun1();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }else {
                Snackbar.make(view,"remote service unbinded",Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
