package com.example.ee.aidl;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.design.widget.Snackbar;
import android.util.Log;


public class AidlApp extends Application {
    private final String TAG = "AidlApp";
    public IRemoteService   remoteService;
    private ServiceConnection connection;

    private ICallback callback  = new ICallback.Stub(){

        @Override
        public void fun2() throws RemoteException {
            Log.d(TAG, "fun2 in client ");
        }
    };

    private void bindRemoteService(){
        connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                remoteService = IRemoteService.Stub.asInterface(service);
                try {
                    remoteService.registerCallback(callback);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };

        Intent intent = new Intent(this,RemoteService.class);
        boolean ret = bindService(intent,connection, BIND_AUTO_CREATE);
        if (!ret ){
            Log.e(TAG, "bindRemoteService: bind failed.");
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        bindRemoteService();
    }
}
