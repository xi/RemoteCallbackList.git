// IRemoteService.aidl
package com.example.ee.aidl;

import com.example.ee.aidl.ICallback;

interface IRemoteService {

    int getPid();
    oneway void fun1();
    void registerCallback(ICallback cb);
    void unregisterCallback(ICallback cb);
}
